class profile::defaults {

    # install sudo with default sudoers
    class { sudo: }



	include motd,vim,debian,nrpe

    class { 'aliases':
        admin => $admin_email,
        postmaster => $postmaster_email,
    }
}
