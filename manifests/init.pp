# host based firewall (iptables)
# backup (rdiff-backup)
# trends (collectd)
# log (lumberjack?)
# security (ossec?)
# monitoring (nagio)

class profile (
          $postmaster_email = 'root',
          $admin_email = 'root',
          $backup = true,
          $hbfw = false,
          $inventory = false,
      ) {

    # apply some defaults to the os
    class { profile::osdefaults: }

    # Set admin email in aliases for root

    if $backup == 'true' { 
        class { rdiff_backup: 
            
        }
    }

    if $hbfw == 'true' {
        class { profile::hbfw: }
    }

    if $inventory == 'true' {
        class { profile::inventory: }
    }
}
